<?php
include('layout/header.php');
include('layout/sidebar.php')

?>

<main class="page-content">

    <div class="container-fluid">

        <h2>Insert Provinsi</h2>
        <hr>


        <div class="row">
            <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#staticBackdrop">
                tambah data
            </button>

            <!-- Modal -->
            <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Tambah Provinsi</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="../controller/InputProvinsi.php" method="POST">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="namaProvinsi">Nama provinsi</label>
                                    <input type="text" class="form-control" id="idProvinsi" name="idProf" style="display: none;">
                                    <input type="text" class="form-control" id="namaProvinsi" name="nmprovinsi">
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Modal Delete-->


            <table class="table table-bordered table-striped">
                <thead class="table-dark">
                    <th style="width: 5%;">No</th>
                    <th style="width: 15%;">Action </th>
                    <th style="width: 15%;">Kode Profinsi</th>
                    <th style="width: 65%;">Povinsi</th>
                </thead>
                <tbody>
                    <?php
                    include '../config/koneksi.php';
                    $no = 1;
                    $data = "select * from tb_m_provinsi";
                    $result = mysqli_query($conn, $data);

                    while ($obj = mysqli_fetch_array($result)) {
                    ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td>
                                <button type="button" class="btn btn-sm btn-warning" onclick="btn_edit('<?php echo $obj['kode_provinsi']; ?>','<?php echo $obj['nama_provinsi']; ?>')">EDIT</button> |
                                <a type="button" class="btn btn-sm btn-danger" href="../controller/delete.php?id=<?php echo $obj['kode_provinsi']; ?>">HAPUS</a>

                            </td>
                            <td><?php echo $obj['kode_provinsi']; ?></td>
                            <td><?php echo $obj['nama_provinsi']; ?></td>

                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

    </div>
</main>

<?php
include('layout/footer.php')
?>