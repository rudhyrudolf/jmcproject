<?php
include("layout/header.php");
include("layout/sidebar.php");
include '../config/koneksi.php';

session_start();
?>

<main class="page-content">
    <div class="content-fluid">
        <h2>Insert Kabupaten</h2>
        <hr>

        <div class="row">

            <form action="kabupaten.php" method="GET">
                <div class="form-group">
                    <label for="namaKabupaten">Search</label>
                    <input type="text" class="form-control" id="searchform" name="search">
                </div>
                <button type="submit" class="btn btn-primary mb-3">
                    Search
                </button>

            </form>


        </div>
        <div class="row">


            <br>
            <div>
                <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#staticBackdrop">
                    tambah data
                </button>
                <a type="button" class="btn btn-primary mb-3" onclick="btn_print()">
                    Print
                </a>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Tambah Kabupaten</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="../controller/InputKabupaten.php" method="POST">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="namaKabupaten">Nama Kabupaten</label>
                                    <input type="text" class="form-control" id="idKabupaten" name="idKabupaten" style="display: none;">
                                    <input type="text" class="form-control" id="namaKabupaten" name="namaKabupaten">
                                </div>

                                <div class="form-group">
                                    <label for="namaProvinsi">provinsi</label>
                                    <select class="form-control" id="namaProvinsi" name="nmprovinsi">
                                        <option value="">--All--</option>
                                        <?php
                                        include '../config/koneksi.php';

                                        $query = "select distinct nama_provinsi,kode_provinsi from tb_m_provinsi";
                                        $result = mysqli_query($conn, $query);
                                        while ($d = mysqli_fetch_array($result)) {
                                        ?>
                                            <option value="<?php echo $d['kode_provinsi']; ?>"><?php echo $d['nama_provinsi']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="jmlPend">Jumlah Penduduk</label>
                                    <input type="number" class="form-control" id="jmlPend" name="jmlPend">

                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Modal Delete-->


            <table class="table table-bordered table-striped">
                <thead class="table-dark">
                    <th style="width: 5%;">No</th>
                    <th style="width: 15%;">Action </th>
                    <th style="width: 15%;">Kode Kabupaten</th>
                    <th style="width: 15%;">Nama Kabupaten</th>
                    <th style="width: 35%;">Povinsi</th>
                    <th style="width: 15%;">Jumlah Penduduk</th>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    if (isset($_GET['search'])) {
                        $search = $_GET['search'];
                        $data = "select kb.*,pv.nama_provinsi from tb_m_kabupaten kb JOIN tb_m_provinsi pv ON kb.kode_provinsi = pv.kode_provinsi
                        where nama_kabupaten like '%" . $search . "%' OR nama_provinsi like '%" . $search . "%'";
                        $result = mysqli_query($conn, $data);
                    } else {


                        $data = "select kb.*,pv.nama_provinsi from tb_m_kabupaten kb JOIN tb_m_provinsi pv ON kb.kode_provinsi = pv.kode_provinsi";
                        $result = mysqli_query($conn, $data);
                    }
                    while ($obj = mysqli_fetch_array($result)) {
                    ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td>
                                <button type="button" class="btn btn-sm btn-warning" onclick="btn_editkbpt('<?php echo $obj['kode_kabupaten']; ?>','<?php echo $obj['nama_kabupaten']; ?>','<?php echo $obj['kode_provinsi'] ?>','<?php echo $obj['jumlah_peduduk'] ?>')">EDIT</button> |
                                <a type="button" class="btn btn-sm btn-danger" href="../controller/deleteKabupaten.php?id=<?php echo $obj['kode_kabupaten']; ?>">HAPUS</a>

                            </td>
                            <td><?php echo $obj['kode_kabupaten']; ?></td>
                            <td><?php echo $obj['nama_kabupaten']; ?></td>
                            <td><?php echo $obj['nama_provinsi']; ?></td>
                            <td><?php echo $obj['jumlah_peduduk']; ?></td>



                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</main>

<?php
include("layout/footer.php");
?>