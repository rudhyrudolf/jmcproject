<?php include '../config/koneksi.php';

$provinsi = $_POST['search'];
$countPendudul = 0;
// echo $tes;
?>

<!DOCTYPE html>
<html>

<head>
    <title>LAPORAN JUMLAH PENDUDUK</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

</head>

<body>
    <br>
    <br>
    <div class="container">
        <center>
            <h2>Laporan Jumlah penduduk berdasarkan provinsi dan kabupaten</h2>
            <h4>Pada Tahun 2020</h4>
        </center>

        <br />
        <?php
        if ($provinsi != null) {
            $data = "select sum(jumlah_peduduk) as jml from tb_m_kabupaten kb JOIN tb_m_provinsi pv ON kb.kode_provinsi = pv.kode_provinsi
                        where nama_provinsi like '%" . $provinsi . "%'";
        } else {
            $data = "select sum(jumlah_peduduk) as jml from tb_m_kabupaten kb JOIN tb_m_provinsi pv ON kb.kode_provinsi = pv.kode_provinsi";
        }
        $result = mysqli_query($conn, $data);
        // var_dump(mysqli_fetch_assoc($result)['jml']);
        // return;
        $countPendudul = mysqli_fetch_assoc($result)['jml'];
        ?>
        <p>
            Jumlah Penduduk Provinsi = <?php echo $countPendudul; ?>
        </p>

        <p>
            Jumlah Penduduk Kabupaten
        </p>
        <table class="table table-bordered">
            <thead>
                <th>No</th>
                <th>Kabupaten</th>
                <th>provinsi</th>
                <th>Jumlah Penduduk</th>
            </thead>
            <tbody>
                <?php
                $no = 1;
                if ($provinsi != null) {
                    $data = "select kb.*,pv.nama_provinsi from tb_m_kabupaten kb JOIN tb_m_provinsi pv ON kb.kode_provinsi = pv.kode_provinsi
                                where nama_provinsi like '%" . $provinsi . "%'";
                } else {
                    $data = "select kb.*,pv.nama_provinsi from tb_m_kabupaten kb JOIN tb_m_provinsi pv ON kb.kode_provinsi = pv.kode_provinsi";
                }
                $result = mysqli_query($conn, $data);
                while ($data = mysqli_fetch_array($result)) {
                ?>
                    <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $data['nama_kabupaten']; ?></td>
                        <td><?php echo $data['nama_provinsi']; ?></td>
                        <td><?php echo $data['jumlah_peduduk']; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <script>
        window.print();
    </script>

</body>

</html>