<div class="page-wrapper chiller-theme toggled">
    <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
        <i class="fas fa-bars"></i>
    </a>
    <nav id="sidebar" class="sidebar-wrapper">
        <div class="sidebar-content">
            <div class="sidebar-brand">
                <a href="#">JMC app</a>
                <div id="close-sidebar">
                    <i class="fas fa-times"></i>
                </div>
            </div>

            <!-- sidebar-header  -->

            <div class="sidebar-menu">
                <ul>
                    <li class="header-menu">
                        <span>General</span>
                    </li>
                    <li class="sidebar-dropdown">
                        <a href="http://localhost/JMCapp/view/provinsi.php">
                            <i class="fa fa-globe"></i>
                            <span>Provinsi</span>

                        </a>
                    </li>
                    <li class="sidebar-dropdown">
                        <a href="http://localhost/JMCapp/view/kabupaten.php">
                            <i class="fa fa-globe"></i>
                            <span>Kabupaten</span>
                        </a>

                    </li>

                    <li class="sidebar-dropdown">
                        <a href="#">
                            <i class="fa fa-book"></i>
                            <span>Laporan</span>
                        </a>

                    </li>
                </ul>
            </div>
            <!-- sidebar-menu  -->
        </div>
    </nav>
    <!-- sidebar-wrapper  -->