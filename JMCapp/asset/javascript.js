function btn_edit(id,nmProvinsi)
{
    console.log(id);

    $("#staticBackdrop").modal();
    $("#namaProvinsi").val(nmProvinsi);
    $("#idProvinsi").val(id);
}

function btn_delete(id)
{
    console.log("delete - "+ id);

    $("#staticBackdrop").modal();
}

function btn_editkbpt(id,nmKabupaten,provinsi,peduduk)
{
    $("#staticBackdrop").modal();
    $("#idKabupaten").val(id);
    $("#namaKabupaten").val(nmKabupaten);
    $("#namaProvinsi").val(provinsi);
    $("#jmlPend").val(peduduk);
}

function btn_print()
{
    // alert($('#searchform').val());
    const provinsi = $('#searchform').val();
    
    $.ajax({
        type:"POST",
        url: "../view/printData.php",
        data: {
            search : provinsi
        },
        success: function(result){
            console.log(result);
            var myWindow = window.open("", "MsgWindow");
            myWindow.document.write(result);
        }

    });
}