
-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_provinsi`
--

CREATE TABLE `tb_m_provinsi` (
  `kode_provinsi` int(11) NOT NULL,
  `nama_provinsi` varchar(200) DEFAULT NULL,
  `uniq_provinsi` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_m_provinsi`
--

INSERT INTO `tb_m_provinsi` (`kode_provinsi`, `nama_provinsi`, `uniq_provinsi`) VALUES
(9, 'jawa barat', 'jawabarat'),
(10, 'jawa timur', 'jawatimur'),
(11, 'jawa tengah', 'jawatengah'),
(12, 'DKI jakarta', 'DKIjakarta');
