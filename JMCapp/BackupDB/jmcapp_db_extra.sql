
--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_m_kabupaten`
--
ALTER TABLE `tb_m_kabupaten`
  ADD PRIMARY KEY (`kode_kabupaten`),
  ADD UNIQUE KEY `uniq_kabupaten` (`uniq_kabupaten`),
  ADD KEY `fk_kabupaten1` (`kode_provinsi`);

--
-- Indeks untuk tabel `tb_m_provinsi`
--
ALTER TABLE `tb_m_provinsi`
  ADD PRIMARY KEY (`kode_provinsi`),
  ADD UNIQUE KEY `uniqprovinsi` (`uniq_provinsi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_m_kabupaten`
--
ALTER TABLE `tb_m_kabupaten`
  MODIFY `kode_kabupaten` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_m_provinsi`
--
ALTER TABLE `tb_m_provinsi`
  MODIFY `kode_provinsi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_m_kabupaten`
--
ALTER TABLE `tb_m_kabupaten`
  ADD CONSTRAINT `fk_kabupaten1` FOREIGN KEY (`kode_provinsi`) REFERENCES `tb_m_provinsi` (`kode_provinsi`);
