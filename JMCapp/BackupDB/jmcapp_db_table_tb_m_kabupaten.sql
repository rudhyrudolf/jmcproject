
-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_kabupaten`
--

CREATE TABLE `tb_m_kabupaten` (
  `kode_kabupaten` int(11) NOT NULL,
  `kode_provinsi` int(11) NOT NULL,
  `nama_kabupaten` varchar(63) DEFAULT NULL,
  `jumlah_peduduk` int(11) DEFAULT NULL,
  `uniq_kabupaten` varchar(62) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_m_kabupaten`
--

INSERT INTO `tb_m_kabupaten` (`kode_kabupaten`, `kode_provinsi`, `nama_kabupaten`, `jumlah_peduduk`, `uniq_kabupaten`) VALUES
(2, 11, 'klaten', 250, 'klaten'),
(4, 11, 'wonogiri', 300, 'wonogiri'),
(5, 9, 'cimahi', 350, 'cimahi'),
(6, 12, 'Jakarta Barat', 500, 'JakartaBarat');
